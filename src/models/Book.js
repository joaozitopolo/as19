import mongoose, { Schema } from 'mongoose'
import { gql } from 'apollo-server'

const bookSchema = new Schema({
  name: String,
  owner: { type: Schema.Types.ObjectId, ref: 'owners' }
})

const Book = mongoose.model('books', bookSchema)

const BookTypes = gql`

  type Book {
	  id: ID!
    name: String
    owner: Owner
  }
  
  extend type Query {
    books: [Book]
  }
  
  extend type Mutation {
    addBook(name: String, owner: ID): Book
    removeBook(id: ID!): Book
  }
  
`

const BookQuery = {
  books: () => Book.find({}).populate('owner').exec(),
}

const BookMutation = {
  addBook: (parent, { name, owner }) => Book.create({ name, owner }),
  removeBook: async (parent, args) => Book.findByIdAndDelete(args.id),
}

export { Book, BookTypes, BookQuery, BookMutation }
