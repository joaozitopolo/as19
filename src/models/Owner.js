import mongoose, { Schema } from 'mongoose'
import { gql } from 'apollo-server'

const ownerSchema = new Schema({
  name: String,
})

const Owner = mongoose.model('owners', ownerSchema)

const OwnerTypes = gql`

  type Owner {
	  id: ID!
    name: String
  }
  
  extend type Query {
    owners: [Owner]
  }
  
  extend type Mutation {
    addOwner(name: String): Owner
    removeOwner(id: ID!): Owner
  }
  
`

const OwnerQuery = {
  owners: () => Owner.find({}).exec(),
}

const OwnerMutation = {
  addOwner: (parent, args) => Owner.create(args),
  removeOwner: async (parent, args) => Owner.findByIdAndDelete(args.id),
}

export { Owner, OwnerTypes, OwnerQuery, OwnerMutation }
