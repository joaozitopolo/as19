import mongoose from 'mongoose'

mongoose.Promise = global.Promise

const url = 'mongodb://localhost:27017/admin'
const config = {
    useNewUrlParser: true, 
    user: 'root', 
    pass: 'example', 
    dbName: 'as19'
}

mongoose.connect(url, config);
mongoose.connection.once('open', () => console.log(`Connected to mongo at ${url}`));
