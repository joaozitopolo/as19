import { gql } from 'apollo-server';
import { BookMutation, BookQuery, BookTypes } from '../models/Book';
import { OwnerMutation, OwnerQuery, OwnerTypes } from '../models/Owner';

const TypeDefs = gql`

  type Query
  
  type Mutation
  
`

const typeDefs = [ TypeDefs, BookTypes, OwnerTypes ]

const resolvers = {
  Query: {
    ...BookQuery,
    ...OwnerQuery,
  },
  Mutation: {
    ...BookMutation,
    ...OwnerMutation,
  },
}

export { typeDefs, resolvers };

