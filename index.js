import { ApolloServer } from 'apollo-server';
import { resolvers, typeDefs } from './src/config/modelsConfig';
import './src/config/mongoConfig';

// up Apollo server

const server = new ApolloServer({ typeDefs, resolvers });
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
