# as19

Apollo Server + Docker mongo **2019**

## engagement

- This project want to test the union of modern tools to improve the start of a project using graphql

- The docker runs a pre-configured mongo version

## requirements

- docker
- docker-compose
- node

## principal files

- index.js
	- up the server
- mongoConfig.js
	- db connection
- modelsConfig.js
	- apollo setup of types, queries and mutations, extracted from models
- /src/models/*
	- specific models

## prepare to run

- npm install
- npm install --only=dev

## commands 

- npm run up
	- starts the docker mongo in detached mode.
	- use if you have no local mongo installed
	- accessible by localhost [Mongo Express](http://localhost:8081)
	- (this operation took some seconds to up. Wait a bit before run npm start)
	
- npm start
	- starts the apollo server
	- accessible by localhost [Playground](http://localhost:4000)
	
- npm run down
	- stops the docker mongo

## Playground samples

- insert a new Book

```
mutation {
  addBook(name: "the great book") {
	id
  }
}
```

- find all Books

```
query {
  books {
	id
	name
  }
}
```

- add an owner

```
mutation {
  addOwner(name: "Gatsby Paul") {
    id
    name
  }
}
```

- add a book with owner (owner id
)
```
mutation {
  addBook(name: "test", owner: "5d0f...") { 
    id, name
  }
}
```

- find books with owners

```
query {
  books {
    id
    name
    owner {
      id
      name
    }
  }
}
```
